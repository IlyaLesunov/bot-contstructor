export const GlobalPlugin = {
    install (Vue) {
        Vue.prototype.$Global = {
            dayDateReverse: function (date) {
                if(!date) console.log('Дата не должна быть пустая')
                else {
                    const reverseDate = date.split('-').reverse().join('-')
                    return  reverseDate
                }
            },
            formatHourDate:function (date) {
                if(!date) console.log('Дата не должна быть пустая')
                else {
                    return date.toLocaleTimeString().slice(0,-3)
                }
            }
        }
    }
}
