import axios  from "axios";
import router from "@/router";

export default {
    state: {
        token: localStorage.getItem(`token`) || false
    },
    mutations: {
        setToken(state, token) {
            state.token = token
        },
        clearToken() {
            localStorage.removeItem('token')
            delete axios.defaults.headers.common['Authorization']
        }
    },
    actions: {
        async authUser({commit, dispatch}, dataAuthForm) {
            let apiURL = 'http://localhost:3000/api/entrance/auth/';
            await axios.post(apiURL,  dataAuthForm).then((res) => {
                if (res.status === 200) {
                    commit('setNotification', res)
                    dispatch('setToken', res.data.token)
                    dispatch('isLogin')
                }
            }).catch((error) => {
                commit('setError', error)
            })
        },
        setToken({commit}, token) {
            localStorage.setItem(`token`, JSON.stringify(token))
            const tokenParse = JSON.parse(localStorage.getItem(`token`))
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
            commit('setToken', tokenParse)
        },
        logout({commit}) {
            commit('clearToken')
            router.push('/?=login')
        }
    }
}
