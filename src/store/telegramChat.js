import axios  from "axios";
export default {
    state: {
        telegramChat: {}
    },
    mutations: {
        telegramChat(state, telegramDataChat) {
            state.telegramChat = telegramDataChat
        }
    },
    actions: {
        async getTelegramChat({commit}, dataTelegramCommandO) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegramChat/getChat'
            await axios.get(apiURL, dataTelegramCommandO).then((res) => {
                if(res.status === 200) {
                    console.log('data', res.data)
                    commit('telegramChat', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    },
    getters: {
        telegramChat: c => c.telegramChat || []
    }
}