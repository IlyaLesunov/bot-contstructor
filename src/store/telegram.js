import axios  from "axios";
export default {
    state: {
        telegram: {},
        telegramUser: {}
    },
    mutations: {
        telegramData(state, telegramData) {
            state.telegram = telegramData
        },
        telegramID(state, telegramObject) {
            state.telegramUser = telegramObject
        }
    },
    actions: {
        async sendTelegram({commit}, dataBotObject) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegram/sendTelegram'
            await axios.post(apiURL, dataBotObject).then((res) => {
                if(res.status === 200) {
                    commit('telegramData', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        },
        async saveTelegram({dispatch}, dataTelegramObject) {
            let apiURL = 'http://localhost:3000/api/telegram/saveTelegram'
            await axios.post(apiURL, dataTelegramObject).then((res) => {
                if(res.status === 200) {
                    dispatch('getTelegram', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        },
        async deleteBot({dispatch}, dataBotID) {
            let apiURL = 'http://localhost:3000/api/telegram/delete-bot'
            await axios.post(apiURL, dataBotID).then((res) => {
                if(res.status === 200) {
                    window.location.reload()
                    dispatch('getTelegram', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        },
        async getTelegram({commit}) {
            let apiURL = 'http://localhost:3000/api/telegram/getTelegram'
            await axios.get(apiURL).then((res) => {
                if(res.status === 200) {
                   commit('telegramID', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    },
    getters: {
        telegram: u => u.telegram || [],
        telegramUser: t => t.telegramUser || []
    }
}
