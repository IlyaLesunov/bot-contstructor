import axios  from "axios";

export default {
    actions: {
        async registerUser({commit}, dataAuthForm) {
            console.log(dataAuthForm)
          try {
              let apiURL = 'http://localhost:3000/api/entrance/register/';
              await axios.post(apiURL, dataAuthForm).then((res) => {
                  console.log('Успех')
                  commit('setNotification', res)
              }).catch((error) => {
                  console.log(error)
                  commit('setError', error)
              })
          } catch (e) {
                console.warn(e)
          }
        }
    }
}
