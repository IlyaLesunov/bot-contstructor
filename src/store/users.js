import axios  from "axios";
import router from '../router'
export default {
    state: {
        user: {}
    },
    mutations: {
        user(state, user) {
            state.user = user
        }
    },
    actions: {
       isLogin({commit}) {
            const tokenBody = JSON.parse(localStorage.getItem(`token`))
            axios.defaults.headers.common['Authorization'] = `Bearer ${tokenBody}`
            let apiURL = 'http://localhost:3000/api/users/userID';
            axios.get(apiURL).then((res) => {
                commit('user' , res.data.user)
                router.push(`/user/${res.data.user.login}`)
            }).catch((error) => {
                commit('setError', error)
                router.push('/?=login')
            })
        }
    },
    getters: {
        user: u => u.user || []
    }
}
