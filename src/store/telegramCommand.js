import axios  from "axios";
export default {
    state: {
        commands: {}
    },
    mutations: {
        commandsData(state, objectCommand) {
            state.commands = objectCommand
        }
    },
    actions: {
        async sendTelegramCommand({commit, dispatch}, dataTelegramCommandO) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegramCommand/save'
            await axios.post(apiURL, dataTelegramCommandO).then((res) => {
                if(res.status === 200) {
                    console.log('data', res.data, commit)
                    dispatch('getCommand')
                }
            }).catch((error) => {
                console.log(error)
            })
        },
        async getCommand({commit}) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegramCommand/command'
            await axios.get(apiURL).then((res) => {
                if(res.status === 200) {
                    console.log('data', res.data)
                    commit('commandsData', res.data)
                }
            }).catch((error) => {
                console.log(error)
            })
        },
        async deleteCommand({dispatch}, deleteElementObject) {  //{dispatch},
            let apiURL = 'http://localhost:3000/api/telegramCommand/delete-element'
            await axios.post(apiURL, deleteElementObject).then((res) => {
                if(res.status === 200) {
                    console.log('data', res.data)
                    dispatch('getCommand')
                }
            }).catch((error) => {
                console.log(error)
            })
        }
    },
    getters: {
        commands: u => u.commands || []
    }
}
