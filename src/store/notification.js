export default {
    state: {
        error: {},
        notification: {}
    },
    mutations: {
        setError(state, error) {
            console.log('notification', error)
            state.error = error
        },
        setNotification(state, notification) {
            state.notification = notification
        }
    },
    getters: {
        error: err => err.error || [],
        notification: n => n.notification || []
    }
}
