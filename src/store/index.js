import Vue from 'vue'
import Vuex from 'vuex'
import auth from '@/store/auth'
import users from '@/store/users'
import register from '@/store/register'
import notification from "@/store/notification";
import telegram from "@/store/telegram";
import telegramID from "@/store/telegramID";
import telegramCommand from "@/store/telegramCommand";
import telegramChat from "./telegramChat";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth, register, users, notification, telegram, telegramID, telegramCommand, telegramChat
  }
})
