import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      meta: {
        layout: 'Home'
      },
      component: () => import('../views/Home.vue')
    },
    {
      path: '/user/:id',
      name: 'user/:id',
      meta: {
        requiresAuth: false
      },
      component: () => import('../views/user.vue')
    },
    {
      path: '/user/bot/:id',
      name: 'user/bot/:id',
      meta: {
        requiresAuth: false
      },
      component: () => import('../components/profile/views/bot/botItem')
    }
  ]
})

import store from '/src/store/users'

router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
    if(!store.state.user)
    return next('/?=login')
  } else {
    next()
  }
})

export default router

